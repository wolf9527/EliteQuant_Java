package elitequant.gui;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import elitequant.event.ClientMq;
import elitequant.event.Event;
import elitequant.event.EventHandler;
import elitequant.event.EventType;
import elitequant.event.EventsEngine;
import elitequant.order.OrderStatusEvent;

public class OrderWindow extends JPanel implements EventHandler{
    
    private EventsEngine eventsEngine;
    
    private ClientMq clientMq;
    
    private JTable jTable;
    
    private DefaultTableModel tableModel;
    
    private String[] header = new String[] {"OrderID", "Symbol", "OrderTime", "Status"};
    
    private List<Integer> orderids;
    
    public OrderWindow(EventsEngine eventsEngine, ClientMq clientMq) {
        
        this.eventsEngine = eventsEngine;
        this.clientMq = clientMq;
        orderids = new ArrayList<Integer>();
        
        initTable();
                
        registerEvent();
    }

    private void initTable() {
        setLayout(new BorderLayout());

        tableModel = new DefaultTableModel();
        jTable = new JTable(tableModel) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tableModel.setDataVector(new String[0][0], header);

        jTable.setFillsViewportHeight(true);
        jTable.addMouseListener(new CancelOrderAction());

        JScrollPane scrollPane = new JScrollPane(jTable);
        add(scrollPane);
    }
       
    private class CancelOrderAction extends MouseAdapter{

        @Override
        public void mouseClicked(MouseEvent e) {
            if(e.getClickCount() == 2) {
                int row = jTable.getSelectedRow();
                String  orderId = (String)jTable.getValueAt(row, 0);
                String msg = String.format("c|%s", orderId);
                clientMq.send(msg);
            }
        }
        
    }
    
    private void updateTable(OrderStatusEvent orderEvent) {
        int row = orderids.indexOf(orderEvent.brokerOrderId);
        if(row >= 0) {
            tableModel.setValueAt(orderEvent.orderStatus.name, orderids.indexOf(orderEvent.brokerOrderId), 3);
        }
        else {
            tableModel.addRow(new String[] {
                    String.valueOf(orderEvent.brokerOrderId), 
                    orderEvent.fullSymbol,
                    "0",
                    orderEvent.orderStatus.name});
            orderids.add(orderEvent.brokerOrderId);
        }
    }    
    
    @Override
    public void handle(Event event) {
        if (event.getClass().isAssignableFrom(OrderStatusEvent.class)) {
            updateTable((OrderStatusEvent) event);
        }
        
    }


    private void registerEvent() {
        eventsEngine.registerHandler(EventType.ORDERSTATUS, this);
    }
}
