package elitequant.data;

import java.nio.charset.Charset;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

/**
 * DateFeed baae class
 *
 */
public abstract class DataFeedBase {
	
	/**
	 * subscribe to market data
	 * @param symbols
	 */
	public abstract void subscribeMarketData(String[] symbols);
	
	/**
	 * unsubscribe market data
	 * @param symbols
	 */
	public abstract void unsubscribeMarketData(String[] symbols);
	
	/**
	 * stream next data event
	 */
	public abstract BarEvent streamNext();
	
	protected String httpGet(String url) {
	    return httpGet(url, "UTF-8");
	}
	
	protected String httpGet(String url, String charset) {
	    String data = "";
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);
        try (CloseableHttpResponse response = httpclient.execute(httpGet)) {
            HttpEntity entity = response.getEntity();
            data = EntityUtils.toString(entity, Charset.forName(charset));
        } catch (Exception e) {
            System.out.println(String.format("get http data error, %s", e.getMessage()));
        }
        return data;
	}

}
