package elitequant.data;

public enum TickType {

    TRADE,
    BID,
    ASK
}
