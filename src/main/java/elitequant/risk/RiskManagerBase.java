package elitequant.risk;

import elitequant.order.OrderEvent;

/**
 * RiskManager base class
 * @author btcaimail@163.com
 *
 */
public abstract class RiskManagerBase {
	
	public abstract OrderEvent orderInCompliance(OrderEvent originalOrder);

}
