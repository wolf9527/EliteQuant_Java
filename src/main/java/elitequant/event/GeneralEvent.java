package elitequant.event;

import static elitequant.util.UtilFunc.getDate19700101;

import java.util.Date;

public class GeneralEvent extends Event {
	
	public Date time;
	
	public String content;
	
	public GeneralEvent(){
		super(EventType.GENERAL);
		
		time = getDate19700101();
		content = "";
	}

}
