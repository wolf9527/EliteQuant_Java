package elitequant.event;

public abstract class EventsEngine {
    
    public abstract void put(Event event);
    
    public abstract void registerHandler(EventType eventType, EventHandler handler);
    
}
