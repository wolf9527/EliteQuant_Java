package elitequant.order;

import elitequant.event.Event;
import elitequant.event.EventType;

/**
 * Order event
 *
 */
public class OrderEvent extends Event {
	
    public int internalOrderId;
    
    public int brokerOrderId;
    
    public String fullSymbol;
    
    public OrderType orderType;
    
    public OrderStatus orderStatus;
    
    public double limitPrice;
    
    public double stopPrice;
    
    public int size;         // short < 0, long > 0
    
    public double fillPrice;
    
    public int fillSize;
	
	public OrderEvent() {
		super(EventType.ORDER);
		
		internalOrderId = -1;
		brokerOrderId = -1;
		fullSymbol = "";
		orderType = OrderType.MARKET;
		orderStatus = OrderStatus.NONE;
	}

}
