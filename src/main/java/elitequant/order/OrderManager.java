package elitequant.order;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Manage/track all the orders
 *
 */
public class OrderManager {

    private int internalOrderId;                   // unique internal_orderid
    private Map<Integer, OrderEvent> orderDict;               // internal_order to [# sent, # filled, is_canceled,
    
    public OrderManager() {
        orderDict = new HashMap<Integer, OrderEvent>();
	}
    
    public synchronized void placeOrder(OrderEvent orderEvent){
        if(orderEvent.internalOrderId < 0){  // internalOrderId not yet assigned
        	orderEvent.internalOrderId = internalOrderId;
        	internalOrderId += 1;
        	orderDict.put(orderEvent.internalOrderId, orderEvent);
        }
   	
    }
}
