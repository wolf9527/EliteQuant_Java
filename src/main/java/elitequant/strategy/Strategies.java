package elitequant.strategy;

import elitequant.data.TickEvent;

/**
 * Strategies is a collection of strategy
 * Usage e.g.: strategy = Strategies(strategyA, DisplayStrategy())
 * 
 * @author btcaimail@163.com
 *
 */
public class Strategies extends StrategyBase {
    
    private StrategyBase[] strategiesCollection;

    public Strategies(StrategyBase ... strategies) {
        super(null, null);
        strategiesCollection = strategies;
    }

    @Override
    public void onTick(TickEvent tickEvent) {
        for(StrategyBase strategy : strategiesCollection ) {
            strategy.onTick(tickEvent);
        }
    }
    
    
}
