package elitequant.strategy.mystrategy;

import elitequant.data.BarEvent;
import elitequant.event.EventsEngine;
import elitequant.order.OrderEvent;
import elitequant.order.OrderType;
import elitequant.strategy.StrategyBase;

/**
 * buy on the first tick then hold to the end
 * 
 */
public class BuyAndHoldStrategy extends StrategyBase {
	
    private boolean invested;
    
    private int ticks;

    public BuyAndHoldStrategy(String[] symbols, EventsEngine eventsEngine) {
        super(symbols, eventsEngine);
	}
    
	@Override
	public void onBar(BarEvent barEvent) {
        String symbol = symbols[0];
        if(symbol.equals(barEvent.fullSymbol)){
            if(!invested){
            	OrderEvent o = new OrderEvent();
                o.fullSymbol = symbol;
                o.orderType = OrderType.MARKET;
                o.size = 100;
                placeOrder(o);
                invested = true;
            }
        }
        ticks += 1;
	}

}
