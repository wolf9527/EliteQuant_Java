package elitequant.strategy;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import elitequant.data.BarEvent;
import elitequant.data.TickEvent;
import elitequant.event.EventsEngine;
import elitequant.order.OrderEvent;

/**
 * Base strategy class
 * 
 */
public abstract class StrategyBase {
	
	protected String[] symbols;
    
	protected EventsEngine eventsEngine;
	
	private String name;
	private String author;
	private boolean initialized;
	
	public StrategyBase(String[] symbols, EventsEngine eventsEngine) {
	    this.symbols = symbols;
	    this.eventsEngine = eventsEngine;
	    
	    name = "";
	    author = "";
	    initialized = false;
	}
	
	public void onStart(){
		initialized = true;
	}
	
	public void onStop(){
		initialized = false;
	}
	
	/**
	 * Respond to tick
	 */
	public void onTick(TickEvent tickEvent){
		
	}
	
	/**
	 * Respond to bar
	 * @param bar
	 */
	public void onBar(BarEvent barEvent){
		
	}
	
	/**
	 * on order acknowledged
	 */
	public void onOrder(){
		
	}
	
	/**
	 * on order canceled
	 */
	public void onCancel(){
		
	}
	
	/**
	 * on order filled
	 */
	public void onFill(){
		
	}
	
	protected void placeOrder(OrderEvent orderEvent){
	    eventsEngine.put(orderEvent);
	}
	
	protected void cancelOrder(String oid){
		// TODO:
	}
	
	public static List<Class<StrategyBase>> strategyClassList(){
        List<Class<StrategyBase>> list = new ArrayList<Class<StrategyBase>>();
        try {

    		ClassLoader cl = Thread.currentThread().getContextClassLoader();
    		URL url = cl.getResource("elitequant/strategy/mystrategy");
    		File fileDir = new File(url.getFile());
    		for(File file : fileDir.listFiles()) {
    			String fileName = file.getName();
    			fileName = fileName.substring(0, fileName.length()-6);
                Class clazz = Class.forName(String.format("elitequant.strategy.mystrategy.%s", fileName));
                if(clazz.isAssignableFrom(StrategyBase.class)) {
        			list.add(clazz);	
                }	
    		}	
        }
        catch(Exception e) {
        	System.out.println(String.format("load strategy list error:%s", e.getMessage()));
        }
		return list;
	}

}
