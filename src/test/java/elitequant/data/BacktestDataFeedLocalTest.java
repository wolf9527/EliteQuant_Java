package elitequant.data;

public class BacktestDataFeedLocalTest {

    
    public static void main(String[] args) {
        BacktestDataFeedLocal dataFeed = new BacktestDataFeedLocal("D:\\projects\\EliteQuant_Python\\hist", "2014-12-03", "2014-12-05");
        dataFeed.subscribeMarketData(new String[] {"AMZN"});
        BarEvent bar = null;
        while((bar = dataFeed.streamNext()) != null) {
            System.out.println(bar);
        }

    }

}
